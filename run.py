#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import getcwd, path
from typing import Optional
from argparse import ArgumentParser
from yaml import load, Loader
from datrool_lib.validators import DictValidator
from datrool_lib.logger import set_logger
config_validator = {
	"workdir": (str, lambda x, *a: path.isdir(x)),
	"exchanges": (list, {
		"name": (str, None),
		"symbols": ((list, Optional), str),
		"intervals": ((list, Optional), str)
	}),
	"archiving": ((dict, Optional), {
		"interval": (str, lambda x, *a: x in ["hourly", "daily", "weekly"]),
		"minute": ((int, Optional), lambda x, *a: x >= 0 and x < 60),
		"hour": ((int, Optional), lambda x, *a: x >= 0 and x < 24),
		"weekday": ((str, Optional), lambda x, *a: x in ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"])
	}),
	"loggers": ([list, Optional], {
			"name": (str, None),
			"screen": (bool, None),
			"path": ([str, Optional], lambda x, *a: path.isdir(x)),
			"level": ([int, Optional], None),
			"formatter": ([str, Optional], None)
	})
}


def entrypoint(local=False):
	if local:
		from src.collector import Collector
	else:
		from datrool_collector.collector import Collector  # type: ignore

	parser = ArgumentParser(
		description="Utility collecting and maintain raw exchange data for strategy testing"
	)
	parser.add_argument(
		"command",
		nargs=1,
		default=["collect"],
		help="CLI command (collect, pack, unpack, aggregate)"
	)
	parser.add_argument(
		"--config",
		nargs="?",
		default="%s/config.yaml" % getcwd(),
		help="CryptoBOT working directory (default actual directory)"
	)
	parser.add_argument("-g", "--graph", action="store_true", help="Collect initial graph")
	args = parser.parse_args()
	try:
		cf = open(args.config, "r")
		raw = cf.read()
		cf.close()
	except Exception:
		print("ERROR: Cannot read configuration file `%s`" % args.config)
		return

	try:
		config = load(raw, Loader=Loader)
		if not DictValidator.check(config, config_validator):
			raise Exception()
	except Exception:
		print("ERROR: Invalid yaml configuration file `%s`" % args.config)
		return

	if "loggers" in config:
		try:
			for logger in config["loggers"]:
				set_logger(**logger)
		except Exception:
			print("ERROR: Invalid logging configuration")
			return

	match args.command[0]:
		case "collect":
			Collector(config, load_graph=args.graph)
		case "pack":
			pass
		case "unpack":
			pass
		case "aggregate":
			pass
		case _:
			print("ERROR: invalid command `%s`" % args.command[0])


if __name__ == "__main__":
	entrypoint(local=True)
