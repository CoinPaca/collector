# datrool / collector

Utility collecting and maintain raw exchange data for strategy testing.

## Installation

TODO

## Configuration

TODO

## Using

### Collecting data
### Packing & unpacking data
### Aggregating packages


## Development initialization
1. clone this repository
2. enter repository
3. initialize and activate virtualenv
```sh
python -m venv venv
source venv/bin/activate
```
4. intall requirements.txt and dev_requirements.txt
```sh
python -m pip install -r requirements.txt
python -m pip install -r requirements_dev.txt
```
5. to run utility use `run.py` script located in project root
```sh
python ./run.py --collect --config <path-to-your-yaml-config>
```

## Roadmap
- finish README
- create project structure
- implement data collector
- implement data packer
- implement packed data aggregator


## License

Created by Patrik Katreňák and released under General Public License v3.0.
