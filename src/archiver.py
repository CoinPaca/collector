# -*- coding: utf-8 -*-
from os import getcwd, listdir, chdir, rename, remove, mkdir, rmdir
from os.path import isfile, join
from shutil import move
from re import sub
from time import time
from asyncio import sleep
from tarfile import open as tar_open
from multiprocessing import Process
from logging import getLogger
from orjson import loads
from datrool_lib.scheduler import Scheduler


class Archiver:
	def __init__(self, config, collector=None):
		self.__log = getLogger("COLLECTOR")
		self.__config = config
		self.__path = sub(r"/$", "", config["workdir"])
		self.__collector = collector
		self.__schedule = Scheduler()
		self.__pending = False
		self.__busy = False

		if collector and "archiving" in self.__config:
			sched = {
				"minute": self.__config["archiving"].get("minute", 0)
			}
			if self.__config["archiving"]["interval"] in ["daily", "weekly"]:
				sched["hour"] = self.__config["archiving"].get("hour", 0)
			if self.__config["archiving"]["interval"] == "weekly":
				sched["weekday"] = self.__config["archiving"].get("weekday", "sunday")
			# TODO: async scheduler
			self.__schedule.every(self.archive, **sched)
		if "aggregate" in self.__config:
			for exchange_name in self.__config["exchanges"].keys():
				self.aggregate_archives(exchange_name)
		if "unpack" in self.__config:
			for (exchange_name, val) in self.__config["exchanges"].items():
				self.unpack(exchange_name, val["symbols"])

	async def archive(self):
		if self.__collector and self.__collector.busy:
			self.__pending = True
			return
		self.__busy = True
		for exchange in self.__config["exchanges"]:
			archiver = Process(
				target=self.pack,
				args=(exchange["name"])
			)
			archiver.start()
			while archiver.is_alive():
				await sleep(1)
		self.__busy = False
		if self.__collector and self.__collector.pending:
			await self.__collector.write_stream()

	def pack(self, exchange_name):
		src = "%s/%s/collector/" % (self.__path, exchange_name)
		dest = "%s/%s/packed/" % (self.__path, exchange_name)
		temp = "%s/%s/tmp/" % (self.__path, exchange_name)

		timestamp = round(time())
		graph_files = [f for f in listdir(src) if (
			isfile(join(src, f))
			and "graph" in f)
		]
		if len(graph_files):
			tar = tar_open("%s/%s_graph.tar.gz" % (dest.replace("packed/", ""), timestamp), "w:gz")
			cwd = getcwd()
			chdir(src)
			for file in graph_files:
				tar.add(file)
				remove(file)
			tar.close()
			chdir(cwd)

		stream_files = graph_files = [f for f in listdir(src) if (
			isfile(join(src, f))
			and "stream" in f)
		]
		for file_name in stream_files:
			stream_files.append(file_name.replace(".txt", ".json"))
			file = open(join(src, file_name), "r")
			txt = file.read()
			file.close()
			remove(join(src, file_name))
			while len(txt) and txt[0] != "{":
				txt = txt[1:]
			json = "[" + txt + "]" if len(txt) else "[]"
			file = open(temp + file_name.replace(".txt", ".json"), "w")
			txt = file.write(json)
			file.close()
		if len(stream_files):
			tar = tar_open("%s/%s_stream.tar.gz" % (dest, timestamp), "w:gz")
			cwd = getcwd()
			chdir(temp)
			for file in stream_files:
				tar.add(file)
			tar.close()
			for file in stream_files:
				remove(file)
			chdir(cwd)

	def unpack(self, exchange_name, symbols):
		source_path = "%s/data/%s/packed" % (self.__config["workdir"], exchange_name)
		destination_path = "%s/data/%s/raw" % (self.__config["workdir"], exchange_name)
		tmp_path = "%s/data/%s/tmp" % (self.__config["workdir"], exchange_name)
		backup_path = "%s/data/%s/backup" % (self.__config["workdir"], exchange_name)
		mkdir(tmp_path)
		archives = [f for f in listdir(source_path) if (
			isfile(join(source_path, f))
			and f[-6:] in ["tar.gz", "tar.xz"])
		]
		for archive_name in archives:
			timestamp = archive_name.split("_")[0]
			archive = tar_open(source_path + archive_name, "r:*")
			archive.extractall(tmp_path)
			for f in listdir(tmp_path) if isfile(join(tmp_path, f)):
				if not self.check_validity(join(tmp_path, f)):
					self.__log.error("\n---\nValidity check failed for archive: %s" % (source_path + archive_name))
					self.__log.error("File: %s---\n" % f)
					return
				move(join(tmp_path, f), "%s/%s_%s" % (destination_path, timestamp, f))
			move(join(source_path, archive_name), backup_path)
		rmdir(tmp_path)

	def aggregate_archives(self, exchange_name):
		cwd = getcwd()
		path = "%s/data/%s/packed" % (self.__config["workdir"], exchange_name)
		tmp_path = "%s/tmp" % path
		aggregate_path = "%s/agg" % path
		chdir(path)
		archives = [f for f in listdir(path) if (
			isfile(join(path, f))
			and f[-6:] in ["tar.gz", "tar.xz"]
			and "graph" not in f
			and "aggregated" not in f)
		]
		archives.sort()
		mkdir(tmp_path)
		mkdir(aggregate_path)
		timestamp = None
		counter = 0
		for archive_name in archives:
			if timestamp is None:
				timestamp = archive_name.split("_")[0]
			archive = tar_open(path + archive_name, "r:*")
			archive.extractall(tmp_path)
			for f in listdir(tmp_path) if isfile(join(tmp_path, f)):
				counter += 1
				with open(join(tmp_path, f), "r") as source:
					data = source.read()
					prefix = ""
				if not isfile(aggregate_path, f):
					prefix = "["
				with open((aggregate_path, f), "a") as dest:
					dest.write(prefix)
					if len(data) > 2:
						dest.write(",")
					dest.write(data[1:-1])
					if counter == self.__config["aggregate"]["factor"]:
						dest.write("]")
				if not self.check_validity("%s/%s" % (aggregate_path, f)):
					self.__log.error("\n---\nValidity check failed for archive: %s" % (aggregate_path + archive_name))
					self.__log.error("File: %s---\n" % f)
					return
				remove(join(tmp_path, f))
			if counter == self.__config["aggregate"]["factor"]:
				chdir(aggregate_path)
				tar = tar_open("%s/%s_stream_aggregated.tar.xz" % (path, timestamp), "w:xz")
				for f in listdir(aggregate_path) if isfile(join(aggregate_path, f)):
					tar.add(f)
					remove(f)
				tar.close()
				timestamp = None
				chdir(path)
		rmdir(tmp_path)
		rmdir(aggregate_path)
		chdir(cwd)

	def check_validity(self, json_file):
		with open(json_file, "rb") as json:
			try:
				loads(json.read())
			except Exception as e:
				self.__log.error("Exception: %s" % str(e))
				return False
			return True

	@property
	def pending(self):
		return self.__pending

	@property
	def busy(self):
		return self.__busy
