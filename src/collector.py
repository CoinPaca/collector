# -*- coding: utf-8 -*-
from os import path, mkdir
from re import sub
from time import time
from asyncio import sleep, get_event_loop, gather
from multiprocessing import Process
from logging import getLogger
from orjson import loads, dumps
from datrool_exchanges import load_exchange
from .archiver import Archiver


class Collector:
	def __init__(self, config: dict, load_graph=False):
		self.__log = getLogger("COLLECTOR")
		self.__config = config
		self.__path = sub(r"/$", "", config["workdir"])
		self.__load_graph = load_graph
		self.__archiver = Archiver(config, collector=self)
		self.__pending = False
		self.__busy = False
		self.__buffer = {}
		loop = get_event_loop()
		loop.run_until_complete(self.start())

	async def start(self):
		coroutines = []
		for item in self.__config["exchanges"]:
			self.__buffer[item["name"]] = []
			if not self.__check_directory("%s/%s" % (self.__path, item["name"])):
				return
			exchange = load_exchange(item["name"])(
				symbols=item.get("symbols", []),
				intervals=item.get("intervals", []),
				credentials=item.get("intervals", [])
			)
			coroutines.append(self.process_exchange(exchange))
		await gather(self.__schedule_write(), *coroutines)

	async def process_exchange(self, exchange):
		p = []
		if self.__load_graph:
			for symbol in exchange.symbols:
				p.append(Process(target=self.write_graph, args=(
					exchange, symbol, exchange.intervals
				)))
				p[-1].start()
			while len([1 for i in p if i.is_alive]) != len(p):
				await sleep(0.2)
		await exchange.subscribe(self.buffer_stream, raw=True)

	def write_graph(self, exchange, symbol: str, intervals: list):
		graph = exchange.get_graph(symbol, intervals, 1000, raw=True)
		data = dumps(graph)
		with open("%s/%s/collector/%s_graph.json" % (self.__path, exchange.name, symbol), "wb+") as fd:
			fd.write(data)

	async def buffer_stream(self, data, exchange, *args, **kwargs):
		print("data received")
		self.__buffer[exchange.name].append(data)

	async def write_stream(self):
		if self.__archiver.busy:
			self.__pending = True
			return
		self.__busy = True
		p = []
		for exchange in self.__buffer:
			p.append(
				Process(
					target=self.write,
					args=(
						self.__buffer[exchange],
						"%s/%s" % (self.__path, exchange),
					)
				)
			)
			self.__buffer[exchange] = []
			p[-1].start()
		while len([1 for i in p if i.is_alive]) != len(p):
			await sleep(1)
		self.__busy = False
		if self.__archiver.pending:
			await self.__archiver.archive()

	@staticmethod
	def write(data, p):
		prepared = {}
		for item in data:
			symbol = loads(item.encode())["data"]["s"]
			if not prepared.get(symbol):
				prepared[symbol] = []
			prepared[symbol].append(item)

		for symbol in prepared:
			append = False
			if path.isfile("%s/collector/%s_stream.txt" % (p, symbol)):
				append = True
			txt = ",".join(prepared[symbol])

			file = open("%s/collector/%s_stream.txt" % (p, symbol), "ab")
			file.write((",%s" % txt if append else txt).encode())
			file.close()

	@property
	def busy(self):
		return self.__busy

	def __check_directory(self, workdir) -> bool:
		paths = [
			workdir,
			"%s/raw" % workdir,
			"%s/packed" % workdir,
			"%s/collector" % workdir,
			"%s/graph" % workdir,
			"%s/tmp" % workdir
		]
		for p in paths:
			if not path.isdir(p):
				try:
					mkdir(p)
				except Exception:
					self.__log.error("Cannot create folder %s in path %s" % self.__config["workdir"])
					return False
		return True

	async def __schedule_write(self):
		while True:
			print("will sleep", 59 - time() % 60)
			await sleep(59 - time() % 60)
			await self.write_stream()
			await sleep(1)
